package ru.tsc.ichaplygina.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.ichaplygina.taskmanager.api.repository.ISessionRepository;
import ru.tsc.ichaplygina.taskmanager.model.Session;
import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

import java.util.ArrayList;
import java.util.List;

public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private ISessionRepository sessionRepository;

    @Before
    public void initRepository() {
        sessionList = new ArrayList<>();
        sessionRepository = new SessionRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            sessionRepository.add(session);
            sessionList.add(session);
        }
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Session newSession = new Session();
        sessionRepository.add(newSession);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<Session> sessionList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            sessionList.add(new Session());
        }
        sessionRepository.addAll(sessionList);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testAddAllNull() {
        sessionRepository.addAll(null);
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        sessionRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Session> actualSessionList = sessionRepository.findAll();
        Assert.assertEquals(sessionList, actualSessionList);
    }

    @Test
    public void testFindByIdPositive() {
        for (@NotNull final Session session : sessionList) {
            Assert.assertNotNull(sessionRepository.findById(session.getId()));
            Assert.assertEquals(session, sessionRepository.findById(session.getId()));
        }
    }

    @Test
    public void testFindByIdNegative() {
        @NotNull final String id = NumberUtil.generateId();
        Assert.assertNull(sessionRepository.findById(id));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testIsEmpty() {
        Assert.assertFalse(sessionRepository.isEmpty());
        @NotNull ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.isEmpty());
        sessionRepository.clear();
        Assert.assertTrue(sessionRepository.isEmpty());
    }

    @Test
    public void testRemovePositive() {
        for (@NotNull final Session session : sessionList) {
            sessionRepository.remove(session);
            Assert.assertNull(sessionRepository.findById(session.getId()));
        }
    }

    @Test
    public void testRemoveNegative() {
        @NotNull final Session sessionNotFromRepository = new Session();
        sessionRepository.remove(sessionNotFromRepository);
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIdPositive() {
        for (@NotNull final Session session : sessionList) {
            Assert.assertNotNull(sessionRepository.removeById(session.getId()));
            Assert.assertNull(sessionRepository.findById(session.getId()));
        }
    }

    @Test
    public void testRemoveByIdNegative() {
        @NotNull final String idNotFromRepository = NumberUtil.generateId();
        Assert.assertNull(sessionRepository.removeById(idNotFromRepository));
    }

}
