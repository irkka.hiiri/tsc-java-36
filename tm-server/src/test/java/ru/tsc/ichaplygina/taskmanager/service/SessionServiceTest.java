package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.ichaplygina.taskmanager.api.repository.ISessionRepository;
import ru.tsc.ichaplygina.taskmanager.api.repository.IUserRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.ISessionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IncorrectCredentialsException;
import ru.tsc.ichaplygina.taskmanager.exception.security.AccessDeniedException;
import ru.tsc.ichaplygina.taskmanager.model.Session;
import ru.tsc.ichaplygina.taskmanager.repository.SessionRepository;
import ru.tsc.ichaplygina.taskmanager.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class SessionServiceTest {

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private List<Session> sessionList;

    @Before
    public void initTest() {
        @NotNull final ISessionRepository sessionRepository = new SessionRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IUserService userService = new UserService(userRepository, propertyService);
        userService.add("admin", "admin", "admin@admin", Role.ADMIN, "A.", "D.", "Min");
        userService.add("user", "user", "user@user", Role.USER, "U.", "S.", "Er");
        userService.add("cat", "cat", "cat@cat", Role.USER, "C.", "A.", "T");
        userService.add("mouse", "mouse", "mouse@mouse", Role.USER, "M.", "O.", "Use");
        userService.lockByLogin("mouse");
        sessionService = new SessionService(sessionRepository, propertyService, userService);
        sessionList = new ArrayList<>();
        @NotNull final Session adminSession = new Session();
        adminSession.setUserId(userService.findByLogin("admin").getId());
        @NotNull final Session userSession = new Session();
        userSession.setUserId(userService.findByLogin("user").getId());
        sessionService.add(adminSession);
        sessionList.add(adminSession);
        sessionService.add(userSession);
        sessionList.add(userSession);
    }

    @Test
    public void testAdd() {
        Assert.assertEquals(2, sessionService.getSize());
        @NotNull Session session = new Session();
        sessionService.add(session);
        Assert.assertEquals(3, sessionService.getSize());
    }

    @Test
    public void testAddAll() {
        @NotNull List<Session> sessionList = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            sessionList.add(new Session());
        }
        sessionService.addAll(sessionList);
        Assert.assertEquals(12, sessionService.getSize());
    }

    @Test
    public void testClear() {
        sessionService.clear();
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testCloseSession() {
        @NotNull final Session session = sessionService.findAll().get(0);
        sessionService.closeSession(session);
        Assert.assertEquals(1, sessionService.getSize());
        Assert.assertNull(sessionService.findById(session.getId()));
    }

    @Test
    public void testFindAll() {
        @NotNull List<Session> sessionList = sessionService.findAll();
        Assert.assertEquals(2, sessionList.size());
    }

    @Test
    public void testFindById() {
        @NotNull final String sessionId = sessionService.findAll().get(0).getId();
        Assert.assertEquals(sessionList.get(0), sessionService.findById(sessionId));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(2, sessionService.getSize());
        sessionService.add(new Session());
        Assert.assertEquals(3, sessionService.getSize());
        sessionService.clear();
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testIsEmpty() {
        Assert.assertFalse(sessionService.isEmpty());
        sessionService.clear();
        Assert.assertTrue(sessionService.isEmpty());
    }

    @Test
    public void testOpenSession() {
        @NotNull final Session session = sessionService.openSession("cat", "cat");
        Assert.assertEquals(3, sessionService.getSize());
        Assert.assertNotNull(session.getSignature());
    }

    @Test(expected = LoginEmptyException.class)
    public void testOpenSessionEmptyLogin() {
        sessionService.openSession("", "");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testOpenSessionEmptyPassword() {
        sessionService.openSession("cat", "");
    }

    @Test(expected = IncorrectCredentialsException.class)
    public void testOpenSessionIncorrectPassword() {
        sessionService.openSession("cat", "dog");
    }

    @Test(expected = IncorrectCredentialsException.class)
    public void testOpenSessionIncorrectLogin() {
        sessionService.openSession("dog", "dog");
    }

    @Test(expected = AccessDeniedException.class)
    public void testOpenSessionLockedUser() {
        sessionService.openSession("mouse", "mouse");
    }

    @Test
    public void testValidatePrivileges() {
        sessionService.validatePrivileges(sessionList.get(0).getUserId());
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidatePrivilegesAccessDenied() {
        sessionService.validatePrivileges(sessionList.get(1).getUserId());
    }

    @Test
    public void testValidateSession() {
        @NotNull final Session session = sessionService.openSession("cat", "cat");
        sessionService.validateSession(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateSessionNull() {
        sessionService.validateSession(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateSessionNoSignature() {
        sessionService.validateSession(new Session());
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateSessionBadSignature() {
        @NotNull Session session = new Session();
        session.setId(sessionList.get(0).getId());
        session.setUserId(sessionList.get(0).getUserId());
        session.setTimeStamp(sessionList.get(0).getTimeStamp());
        session.setSignature("lalala");
        sessionService.validateSession(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateSessionNotExist() {
        @NotNull Session session = sessionService.openSession("cat", "cat");
        sessionService.removeById(session.getId());
        sessionService.validateSession(session);
    }

}
