package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.api.repository.IUserRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.*;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.model.User;
import ru.tsc.ichaplygina.taskmanager.repository.ProjectRepository;
import ru.tsc.ichaplygina.taskmanager.repository.TaskRepository;
import ru.tsc.ichaplygina.taskmanager.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class ProjectServiceTest {

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IUserService userService;

    @NotNull
    private IProjectTaskService projectTaskService;

    @NotNull
    private List<Task> taskList;

    @Before
    public void initTest() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        userService = new UserService(userRepository, propertyService);
        projectService = new ProjectService(projectRepository, userService);
        taskService = new TaskService(taskRepository, userService);
        projectTaskService = new ProjectTaskService(taskService, projectService);
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        @NotNull final User admin = new User("admin", "admin", "admin@admin", "A.", "D.", "Min", Role.ADMIN);
        @NotNull final User user = new User("user", "user", "user@user", "U.", "S.", "Er", Role.USER);
        userService.add(admin);
        userService.add(user);
        projectList.add(new Project("Admin Project 1", "", admin.getId()));
        projectList.add(new Project("Admin Project 2", "", admin.getId()));
        projectList.add(new Project("User Project 1", "", user.getId()));
        projectList.add(new Project("User Project 2", "", user.getId()));
        taskList.add(new Task("Admin Task 1", "", admin.getId()));
        taskList.add(new Task("Admin Task 2", "", admin.getId()));
        taskList.add(new Task("User Task 1", "", user.getId()));
        taskList.add(new Task("User Task 2", "", user.getId()));
        for (@NotNull final Project project : projectList) projectService.add(project);
        for (@NotNull final Task task : taskList) taskService.add(task);
        for (int i = 0; i < 4; i++)
            taskService.addTaskToProject(admin.getId(), taskList.get(i).getId(), projectList.get(i).getId());
    }

    @Test
    public void testAdd() {
        Assert.assertEquals(4, projectService.getSize());
        projectService.add("123", "123", "123");
        Assert.assertEquals(5, projectService.getSize());
    }

    @Test
    public void testAddAll() {
        @NotNull List<Project> projectList = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            projectList.add(new Project());
        }
        projectService.addAll(projectList);
        Assert.assertEquals(14, projectService.getSize());
    }

    @Test
    public void testClearAdmin() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String adminUserId = userService.findByLogin("user").getId();
        projectService.clear(adminUserId);
        Assert.assertEquals(0, projectService.getSize(adminUserId));
        Assert.assertEquals(0, projectService.getSize(userId));
    }

    @Test
    public void testClearUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String adminUserId = userService.findByLogin("admin").getId();
        projectService.clear(userId);
        Assert.assertEquals(0, projectService.getSize(userId));
        Assert.assertEquals(2, projectService.getSize(adminUserId));
    }

    @Test
    public void testCompleteById() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNotNull(projectService.completeById(userId, projectList.get(0).getId()));
        Assert.assertEquals(Status.COMPLETED, projectService.findById(userId, projectList.get(0).getId()).getStatus());
    }

    @Test
    public void testCompleteByIndex() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNotNull(projectService.completeByIndex(userId, 0));
        Assert.assertEquals(Status.COMPLETED, projectService.findById(userId, projectList.get(0).getId()).getStatus());
    }

    @Test
    public void testCompleteByName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNotNull(projectService.completeByName(userId, projectList.get(0).getName()));
        Assert.assertEquals(Status.COMPLETED, projectService.findById(userId, projectList.get(0).getId()).getStatus());
    }

    @Test
    public void testCompleteByIdWrongUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        Assert.assertNull(projectService.completeById(userId, projectList.get(0).getId()));
    }

    @Test
    public void testCompleteByNameWrongUser() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNull(projectService.completeById(userId, projectList.get(0).getName()));
    }

    @Test
    public void testFindAllAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull List<Project> projectList = projectService.findAll(userId);
        Assert.assertEquals(4, projectList.size());
    }

    @Test
    public void testFindAllUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull List<Project> projectList = projectService.findAll(userId);
        Assert.assertEquals(2, projectList.size());
    }

    @Test
    public void testFindByIdAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertEquals(projectList.get(0), projectService.findById(userId, projectId));
    }

    @Test
    public void testFindByIdUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertNull(projectService.findById(userId, projectId));
    }

    @Test
    public void testFindByIndexAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertEquals(projectList.get(0), projectService.findByIndex(userId, 0));
    }

    @Test
    public void testFindByIndexUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        Assert.assertEquals(projectList.get(2), projectService.findByIndex(userId, 0));
    }

    @Test
    public void testFindByNameAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "Admin Project 1";
        Assert.assertEquals(projectList.get(0), projectService.findByName(userId, projectName));
    }

    @Test
    public void testFindByNameUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String projectName = "Admin Project 1";
        Assert.assertNull(projectService.findByName(userId, projectName));
    }

    @Test
    public void testGetIdByIndexAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertEquals(projectList.get(0).getId(), projectService.getId(userId, 0));
    }

    @Test
    public void testGetIdByIndexUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        Assert.assertEquals(projectList.get(2).getId(), projectService.getId(userId, 0));
    }

    @Test
    public void testGetIdByNameAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "Admin Project 1";
        Assert.assertEquals(projectList.get(0).getId(), projectService.getId(userId, projectName));
    }

    @Test
    public void testGetIdByNameUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String projectName = "Admin Project 1";
        Assert.assertNull(projectService.getId(userId, projectName));
    }

    @Test
    public void testGetSizeAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertEquals(4, projectService.getSize(userId));
    }

    @Test
    public void testGetSizeUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        Assert.assertEquals(2, projectService.getSize(userId));
    }

    @Test
    public void testIsEmptyAdmin() {
        @NotNull final String adminUserId = userService.findByLogin("admin").getId();
        @NotNull final String userUserId = userService.findByLogin("user").getId();
        Assert.assertFalse(projectService.isEmpty(adminUserId));
        projectService.clear(adminUserId);
        Assert.assertTrue(projectService.isEmpty(adminUserId));
        Assert.assertTrue(projectService.isEmpty(userUserId));
    }

    @Test
    public void testIsEmptyUser() {
        @NotNull final String adminUserId = userService.findByLogin("admin").getId();
        @NotNull final String userUserId = userService.findByLogin("user").getId();
        Assert.assertFalse(projectService.isEmpty(userUserId));
        projectService.clear(userUserId);
        Assert.assertTrue(projectService.isEmpty(userUserId));
        Assert.assertFalse(projectService.isEmpty(adminUserId));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String adminUserId = userService.findByLogin("admin").getId();
        @NotNull final String userUserId = userService.findByLogin("user").getId();
        @NotNull final String adminProjectId = projectList.get(0).getId();
        @NotNull final String userProjectId = projectList.get(3).getId();
        Assert.assertFalse(projectService.isNotFoundById(adminUserId, adminProjectId));
        Assert.assertFalse(projectService.isNotFoundById(adminUserId, userProjectId));
        Assert.assertFalse(projectService.isNotFoundById(userUserId, userProjectId));
        Assert.assertTrue(projectService.isNotFoundById(userUserId, adminProjectId));
    }

    @Test
    public void testRemovedByIdAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertEquals(projectList.get(0), projectService.removeById(userId, projectId));
    }

    @Test
    public void testRemoveByIdUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertNull(projectService.removeById(userId, projectId));
    }

    @Test
    public void testRemoveByIndexAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertEquals(projectList.get(0), projectService.removeByIndex(userId, 0));
    }

    @Test
    public void testRemoveByIndexUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        Assert.assertEquals(projectList.get(2), projectService.removeByIndex(userId, 0));
    }

    @Test
    public void testRemoveByNameAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "Admin Project 1";
        Assert.assertEquals(projectList.get(0), projectService.removeByName(userId, projectName));
    }

    @Test
    public void testRemoveByNameUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String projectName = "Admin Project 1";
        Assert.assertNull(projectService.removeByName(userId, projectName));
    }

    @Test
    public void testStartById() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNotNull(projectService.startById(userId, projectList.get(0).getId()));
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findById(projectList.get(0).getId()).getStatus());
    }

    @Test
    public void testStartByIndex() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNotNull(projectService.startByIndex(userId, 0));
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findById(projectList.get(0).getId()).getStatus());
    }

    @Test
    public void testStartByName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNotNull(projectService.startByName(userId, projectList.get(0).getName()));
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findById(projectList.get(0).getId()).getStatus());
    }

    @Test
    public void testStartByIdWrongUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        Assert.assertNull(projectService.startById(userId, projectList.get(0).getId()));
    }

    @Test
    public void testStartByNameWrongUser() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNull(projectService.startById(userId, projectList.get(0).getName()));
    }

    @Test
    public void testUpdateById() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertNotNull(projectService.updateById(userId, projectId, "new name", "new description"));
    }

    @Test
    public void testUpdateByIdUnknownId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = "???";
        Assert.assertNull(projectService.updateById(userId, projectId, "new name", "new description"));
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByIdEmptyId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = "";
        projectService.updateById(userId, projectId, "new name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdEmptyName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        projectService.updateById(userId, projectId, "", "new description");
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNotNull(projectService.updateByIndex(userId, 0, "new name", "new description"));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndexWrongIndex() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        projectService.updateByIndex(userId, 666, "new name", "new description");
    }

    @Test
    public void testUpdateStatusById() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertNotNull(projectService.updateStatusById(userId, projectId, Status.PLANNED));
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateStatusByIdEmptyId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = "";
        Assert.assertNotNull(projectService.updateStatusById(userId, projectId, Status.PLANNED));
    }

    @Test
    public void testUpdateStatusByIdUnknownId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = "???";
        Assert.assertNull(projectService.updateStatusById(userId, projectId, Status.PLANNED));
    }

    @Test
    public void testUpdateStatusByIndex() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNotNull(projectService.updateStatusByIndex(userId, 0, Status.PLANNED));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateStatusByIndexWrongIndex() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNotNull(projectService.updateStatusByIndex(userId, 666, Status.PLANNED));
    }

    @Test
    public void testUpdateStatusByName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = projectList.get(0).getName();
        Assert.assertNotNull(projectService.updateStatusByName(userId, projectName, Status.PLANNED));
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateStatusByNameEmptyName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "";
        Assert.assertNotNull(projectService.updateStatusByName(userId, projectName, Status.PLANNED));
    }

    @Test
    public void testUpdateStatusByNameUnknownName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "???";
        Assert.assertNull(projectService.updateStatusByName(userId, projectName, Status.PLANNED));
    }

    @Test
    public void testAddTaskToProject() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        @NotNull final String projectId = projectList.get(1).getId();
        Assert.assertNotNull(projectTaskService.addTaskToProject(userId, projectId, taskId));
    }

    @Test(expected = IdEmptyException.class)
    public void testAddTaskToProjectEmptyTaskId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = "";
        @NotNull final String projectId = projectList.get(1).getId();
        projectTaskService.addTaskToProject(userId, taskId, projectId);
    }

    @Test(expected = IdEmptyException.class)
    public void testAddTaskToProjectEmptyProjectId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        @NotNull final String projectId = "";
        projectTaskService.addTaskToProject(userId, taskId, projectId);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testAddTaskToProjectUnknownProject() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String taskId = taskList.get(2).getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertNotNull(projectTaskService.addTaskToProject(userId, taskId, projectId));
    }

    @Test
    public void testClearProjectsAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        projectTaskService.clearProjects(userId);
        Assert.assertEquals(0, projectService.getSize());
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testClearProjectsUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        projectTaskService.clearProjects(userId);
        Assert.assertEquals(2, projectService.getSize());
        Assert.assertEquals(2, taskService.getSize());
    }

    @Test
    public void testFindAllTasksByProjectId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertEquals(1, projectTaskService.findAllTasksByProjectId(userId, projectId, "").size());
    }

    @Test
    public void testRemoveProjectById() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        projectTaskService.removeProjectById(userId, projectId);
        Assert.assertEquals(3, projectService.getSize(userId));
        Assert.assertEquals(3, taskService.getSize(userId));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveProjectByIdEmptyId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = "";
        projectTaskService.removeProjectById(userId, projectId);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testRemoveProjectByIdBadId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = "123";
        projectTaskService.removeProjectById(userId, projectId);
    }

    @Test
    public void testRemoveProjectByIndex() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        projectTaskService.removeProjectByIndex(userId, 0);
        Assert.assertEquals(3, projectService.getSize(userId));
        Assert.assertEquals(3, taskService.getSize(userId));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveProjectByIndexBadIndex() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        projectTaskService.removeProjectByIndex(userId, 666);
    }

    @Test
    public void testRemoveProjectByName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = projectList.get(0).getName();
        projectTaskService.removeProjectByName(userId, projectName);
        Assert.assertEquals(3, projectService.getSize(userId));
        Assert.assertEquals(3, taskService.getSize(userId));
    }

    @Test(expected = NameEmptyException.class)
    public void testRemoveProjectByNameEmptyName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "";
        projectTaskService.removeProjectByName(userId, projectName);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testRemoveProjectByNameBadName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "123";
        projectTaskService.removeProjectByName(userId, projectName);
    }

    @Test
    public void testRemoveTaskFromProject() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final String taskId = taskList.get(0).getId();
        Assert.assertNotNull(projectTaskService.removeTaskFromProject(userId, projectId, taskId));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveTaskFromProjectIdEmpty() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = "";
        @NotNull final String taskId = taskList.get(0).getId();
        projectTaskService.removeTaskFromProject(userId, projectId, taskId);
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveTaskFromProjectTaskIdEmpty() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final String taskId = "";
        projectTaskService.removeTaskFromProject(userId, projectId, taskId);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testRemoveTaskFromProjectNotFound() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = "234";
        @NotNull final String taskId = taskList.get(0).getId();
        projectTaskService.removeTaskFromProject(userId, projectId, taskId);
    }

}
