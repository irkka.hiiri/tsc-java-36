package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.service.*;

public interface ServiceLocator {

    @NotNull IProjectService getProjectService();

    @NotNull IProjectTaskService getProjectTaskService();

    @NotNull ITaskService getTaskService();

    @NotNull IUserService getUserService();

    @NotNull IPropertyService getPropertyService();

}
